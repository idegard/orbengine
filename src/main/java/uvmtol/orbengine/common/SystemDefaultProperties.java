package uvmtol.orbengine.common;

public interface SystemDefaultProperties {
	final static String ROOT_FOLDER = ".orbengine_jobs";
	final static String MAIN_FILE = "Main.java";
	final static String TEST_FILE = "MainTest.java";
	static final String AUTHOR_FILE = ".author";
	static final String FORKED_FILE = ".forked";
}
