package uvmtol.orbengine.common;

import uvmtol.orbengine.core.entity.Job;

public class JobResponse {

	public static JobResponse view(Job job, String action, String response) {
		return new JobResponse(job, action, response);
	}

	private Job job;
	private String action;
	private String response;

	private JobResponse(Job job, String action, String response) {
		this.action = action;
		this.response = response;
		this.job = job;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
