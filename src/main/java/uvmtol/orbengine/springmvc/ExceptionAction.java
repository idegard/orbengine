package uvmtol.orbengine.springmvc;

import ml.rugal.sshcommon.springmvc.controller.BaseExceptionAction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
@Controller
public class ExceptionAction extends BaseExceptionAction
{

}
