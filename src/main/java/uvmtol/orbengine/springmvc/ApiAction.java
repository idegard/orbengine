package uvmtol.orbengine.springmvc;

import java.io.IOException;
import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import uvmtol.orbengine.common.JobResponse;
import uvmtol.orbengine.core.entity.Job;
import uvmtol.orbengine.core.service.JobService;
import uvmtol.orbengine.core.utils.ErrorCompilacionException;
import uvmtol.orbengine.core.utils.ErrorEjecucionException;
import uvmtol.orbengine.core.utils.ErrorPruebaException;

@Controller
@RequestMapping(value = "/api/v1/job")
public class ApiAction {
	
	private static final Logger LOG = LoggerFactory.getLogger(ApiAction.class.getName());
	
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	
	@Autowired
    private JobService jobService;
	
	
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<JobResponse> newJob(@RequestBody Job jobParam, 
											  @RequestParam(value = "action", required = false) String action,
											  Principal principal){
		try {
			jobParam.setAuthor(principal == null ? "anonymous" : principal.getName());
			jobParam.setForked("");
			return doJob(jobService.save(jobParam), action != null ? action : "compile", false);
		} catch (IOException e) {
			return new ResponseEntity<JobResponse>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<JobResponse> updateJob(@PathVariable("id") String id, 
												 @RequestBody Job jobParam, 
												 @RequestParam(value = "action", required = false) String action,
												 Principal principal){
		
		try {
			Job job = jobService.get(id);
			job.setCode(jobParam.getCode());
			job.setTest(jobParam.getTest());
			if(principal == null && !job.getAuthor().equals("anonymous") || principal != null && !job.getAuthor().equals(principal.getName())){
				job.setAuthor(principal == null ? "anonymous" : principal.getName());
				job.setForked(job.getId());
				job.setId(null);
			}
			jobService.save(job);
			return doJob(job, action != null ? action : "compile", false);
		} catch (IOException e) {
			return new ResponseEntity<JobResponse>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<JobResponse> showJob(@PathVariable("id") String id, @RequestParam(value = "action", required = false) String action){
		try {
			return doJob(jobService.get(id), action != null ? action : "compile", false);
		} catch (IOException e) {
			return new ResponseEntity<JobResponse>(HttpStatus.NOT_FOUND);
		}
	}
	
	private ResponseEntity<JobResponse> doJob(Job job, String action, boolean update){
		try {
			String result = null;
			switch(action){
			  case "execute":
				result = jobService.ejecutar(job);
				break;	
			  case "test":
				result = jobService.probar(job);
				break;
			  default:
				result = jobService.compilar(job) ? "Compilado exitosamente" : "" ;
				break;
			}
			return new ResponseEntity<JobResponse>(JobResponse.view(job, action, result), HttpStatus.OK);
		} catch (ErrorCompilacionException | ErrorEjecucionException | ErrorPruebaException  e) {
			return new ResponseEntity<JobResponse>(JobResponse.view(job, action, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
}
