package uvmtol.orbengine.springmvc;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DefaultAction {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultAction.class.getName());
	
	@RequestMapping("/")
	public ModelAndView home(Principal principal){
		Map<String, String> model = new HashMap<String, String>();
		model.put("usuario", principal == null ? "anonymous" : principal.getName());
		return new ModelAndView("index", model);
	}
	
	@ResponseBody
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ModelAndView homeJob(@PathVariable("id") String id, Principal principal){
		Map<String, String> model = new HashMap<String, String>();
		model.put("usuario", principal == null ? "anonymous" : principal.getName());
		model.put("job", id);
		return new ModelAndView("index", model);
	}
}
