package uvmtol.orbengine.core.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import uvmtol.orbengine.common.SystemDefaultProperties;
import uvmtol.orbengine.core.dao.JobDao;
import uvmtol.orbengine.core.entity.Job;
import uvmtol.orbengine.core.service.JobService;
import uvmtol.orbengine.core.utils.ErrorCompilacionException;
import uvmtol.orbengine.core.utils.ErrorEjecucionException;
import uvmtol.orbengine.core.utils.ErrorPruebaException;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class JobServiceImpl implements JobService{

	@Autowired
    private JobDao jobDao;

	DefaultExecutor executor = new DefaultExecutor();
	ExecuteWatchdog watchdog = new ExecuteWatchdog(15000);
	ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
	
	public JobServiceImpl(){
		executor.setExitValue(0);
		executor.setStreamHandler(streamHandler);
		executor.setWatchdog(watchdog);
	}
	
	@Override
	public Job get(String id) throws IOException {
		return jobDao.get(id);
	}

	@Override
	public Job save(Job job) throws IOException {
		return jobDao.save(job);
	}
		
	@Override
	public boolean compilar(Job job) throws ErrorCompilacionException{
		outputStream.reset();
		CommandLine cmdLine = new CommandLine("javac");
    	cmdLine.addArgument(getCodePath(job));
		try {
			return executor.execute(cmdLine)==0;
		} catch (IOException e) {
			if(watchdog.killedProcess())
				throw new ErrorCompilacionException("El proceso de compilación ha excedido el tiempo máximo permitido");
			throw new ErrorCompilacionException(outputStream.toString().replace(getPath(job), "")); 
		}
	}
	
	@Override
	public String ejecutar(Job job) throws ErrorEjecucionException, ErrorCompilacionException{
		compilar(job);
		outputStream.reset();
		String error = "";
    	for(final File archivo : getParent(job).listFiles()){
    		if(!archivo.getName().contains(".class"))continue;
    		CommandLine cmdLine = new CommandLine("java");
        	cmdLine.addArgument("-cp");
        	cmdLine.addArgument(archivo.getParentFile().getAbsolutePath());
        	cmdLine.addArgument(archivo.getName().replaceAll(".class", ""));
        	try{
        		outputStream.reset();
        		executor.execute(cmdLine);
        		return outputStream.toString();
        	}catch(IOException e){
        		if(watchdog.killedProcess())
    				throw new ErrorEjecucionException("La ejecución ha excedido el tiempo máximo permitido");
        		error = outputStream.toString();
        		continue;
        	}
    	}
    	throw new ErrorEjecucionException(error);
	}
	
	@Override
	public String probar(Job job) throws ErrorCompilacionException, ErrorPruebaException{
		compilar(job);
		outputStream.reset();
		String separator = SystemUtils.IS_OS_WINDOWS?";":":";
		String h = new File(getClass().getClassLoader().getResource("hamcrest.jar").getFile()).getAbsolutePath();
		String j = new File(getClass().getClassLoader().getResource("junit.jar").getFile()).getAbsolutePath();
		CommandLine cmdLine = new CommandLine("javac");
    	cmdLine.addArgument("-cp");
    	cmdLine.addArgument(h+separator+j+separator+getPath(job));
    	cmdLine.addArgument(getTestPath(job));
    	CommandLine cmdLine2 = new CommandLine("java");
    	cmdLine2.addArgument("-cp");
    	cmdLine2.addArgument(h+separator+j+separator+getPath(job));
    	cmdLine2.addArgument("org.junit.runner.JUnitCore");
    	cmdLine2.addArgument("MainTest");
    	try {
			executor.execute(cmdLine);
			executor.execute(cmdLine2);
    	
    	} catch (IOException e) {
    		if(watchdog.killedProcess())
				throw new ErrorPruebaException("El proceso de prueba ha excedido el tiempo máximo permitido");
			throw new ErrorPruebaException(outputStream.toString().replace(getPath(job), "")); 
    	}
    	return outputStream.toString();
	}
	
	private String getCodePath(Job job){
		return new File(SystemDefaultProperties.ROOT_FOLDER+"/"+job.getId()+"/"+SystemDefaultProperties.MAIN_FILE).getAbsolutePath();
	}
	
	private String getTestPath(Job job){
		return new File(SystemDefaultProperties.ROOT_FOLDER+"/"+job.getId()+"/"+SystemDefaultProperties.TEST_FILE).getAbsolutePath();
	}
	
	private String getPath(Job job){
		return getParent(job).getAbsolutePath();
	}
	
	private File getParent(Job job){
		return new File(SystemDefaultProperties.ROOT_FOLDER+"/"+job.getId());
	}
	

}
