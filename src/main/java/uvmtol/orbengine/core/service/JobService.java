package uvmtol.orbengine.core.service;

import java.io.IOException;

import uvmtol.orbengine.core.entity.Job;
import uvmtol.orbengine.core.utils.ErrorCompilacionException;
import uvmtol.orbengine.core.utils.ErrorEjecucionException;
import uvmtol.orbengine.core.utils.ErrorPruebaException;

public interface JobService {
	Job get(String id) throws IOException;
	Job save(Job job) throws IOException;
	boolean compilar(Job job) throws ErrorCompilacionException;
	String ejecutar(Job job) throws ErrorEjecucionException, ErrorCompilacionException;
	String probar(Job job) throws ErrorCompilacionException, ErrorPruebaException;
}
