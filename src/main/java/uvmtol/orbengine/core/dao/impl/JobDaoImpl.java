package uvmtol.orbengine.core.dao.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Repository;

import uvmtol.orbengine.common.SystemDefaultProperties;
import uvmtol.orbengine.core.dao.JobDao;
import uvmtol.orbengine.core.entity.Job;

@Repository
public class JobDaoImpl implements JobDao{
		
	@Override
	public Job get(String id) throws IOException {
		Job job = new Job();
		job.setId(id);
		job.setCode(FileUtils.readFileToString(getFile(id, SystemDefaultProperties.MAIN_FILE), StandardCharsets.UTF_8));
		job.setTest(FileUtils.readFileToString(getFile(id, SystemDefaultProperties.TEST_FILE), StandardCharsets.UTF_8));
		job.setAuthor(FileUtils.readFileToString(getFile(id, SystemDefaultProperties.AUTHOR_FILE), StandardCharsets.UTF_8));
		job.setForked(FileUtils.readFileToString(getFile(id, SystemDefaultProperties.FORKED_FILE), StandardCharsets.UTF_8));
		return job;
	}

	@Override
	public Job save(Job job) throws IOException {
		if(job.getId()==null)job.setId(UUID.randomUUID().toString());
		FileUtils.writeStringToFile(getFile(job.getId(), SystemDefaultProperties.MAIN_FILE), job.getCode(), StandardCharsets.UTF_8);
		FileUtils.writeStringToFile(getFile(job.getId(), SystemDefaultProperties.TEST_FILE), job.getTest(), StandardCharsets.UTF_8);
		FileUtils.writeStringToFile(getFile(job.getId(), SystemDefaultProperties.AUTHOR_FILE), job.getAuthor(), StandardCharsets.UTF_8);
		FileUtils.writeStringToFile(getFile(job.getId(), SystemDefaultProperties.FORKED_FILE), job.getForked(), StandardCharsets.UTF_8);
		return job;
	}
	
	private File getFile(String id, String type){
		return new File(SystemDefaultProperties.ROOT_FOLDER+"/"+id+"/"+type);
	}

}
