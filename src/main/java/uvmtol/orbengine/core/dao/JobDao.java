package uvmtol.orbengine.core.dao;

import java.io.IOException;

import uvmtol.orbengine.core.entity.Job;

public interface JobDao {
	
	Job get(String id) throws IOException;
	
	Job save(Job job) throws IOException;

}
