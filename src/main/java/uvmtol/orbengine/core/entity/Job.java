package uvmtol.orbengine.core.entity;

public class Job {

	String id, code, test, author, forked;

	public Job() {

	}

	public Job(String id, String code, String test, String author, String forked) {
		this.id = id;
		this.code = code;
		this.test = test;
		this.author = author;
		this.forked = forked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getForked() {
		return forked;
	}

	public void setForked(String forked) {
		this.forked = forked;
	}

}
