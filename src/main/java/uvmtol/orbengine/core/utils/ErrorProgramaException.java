package uvmtol.orbengine.core.utils;

public class ErrorProgramaException extends Exception {

	public ErrorProgramaException(String message) {
		super(message);
	}
	
}
