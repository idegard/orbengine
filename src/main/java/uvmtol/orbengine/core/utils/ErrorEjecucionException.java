package uvmtol.orbengine.core.utils;

public class ErrorEjecucionException extends ErrorProgramaException {

	public ErrorEjecucionException(String message) {
		super(message);
	}

}
