package uvmtol.orbengine.core.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.lang3.SystemUtils;

import uvmtol.orbengine.common.SystemDefaultProperties;
import uvmtol.orbengine.core.entity.Job;

public class JobExcecutor{
	
	final DefaultExecutor executor = new DefaultExecutor();
	final ExecuteWatchdog watchdog = new ExecuteWatchdog(15000);
	final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	final PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
	final Job job;
		
	public JobExcecutor(Job job){
		this.job = job;
		executor.setExitValue(0);
		executor.setStreamHandler(streamHandler);
		executor.setWatchdog(watchdog);
	}
	
	public boolean compilar() throws ErrorCompilacionException{
		outputStream.reset();
		CommandLine cmdLine = new CommandLine("javac");
    	cmdLine.addArgument(getCodePath());
		try {
			return executor.execute(cmdLine)==0;
		} catch (IOException e) {
			if(watchdog.killedProcess())
				throw new ErrorCompilacionException("El proceso de compilación ha excedido el tiempo máximo permitido");
			throw new ErrorCompilacionException(outputStream.toString().replace(getPath(), "")); 
		}
	}
	
	public String ejecutar() throws ErrorEjecucionException, ErrorCompilacionException{
		compilar();
		outputStream.reset();
		String error = "";
    	for(final File archivo : getParent().listFiles()){
    		if(!archivo.getName().contains(".class"))continue;
    		CommandLine cmdLine = new CommandLine("java");
        	cmdLine.addArgument("-cp");
        	cmdLine.addArgument(archivo.getParentFile().getAbsolutePath());
        	cmdLine.addArgument(archivo.getName().replaceAll(".class", ""));
        	try{
        		outputStream.reset();
        		executor.execute(cmdLine);
        		return outputStream.toString();
        	}catch(IOException e){
        		if(watchdog.killedProcess())
    				throw new ErrorEjecucionException("La ejecución ha excedido el tiempo máximo permitido");
        		error = outputStream.toString();
        		continue;
        	}
    	}
    	throw new ErrorEjecucionException(error);
	}
	
	public String probar() throws ErrorCompilacionException, ErrorPruebaException{
		compilar();
		outputStream.reset();
		String separator = SystemUtils.IS_OS_WINDOWS?";":":";
		String h = new File(getClass().getClassLoader().getResource("hamcrest.jar").getFile()).getAbsolutePath();
		String j = new File(getClass().getClassLoader().getResource("junit.jar").getFile()).getAbsolutePath();
		CommandLine cmdLine = new CommandLine("javac");
    	cmdLine.addArgument("-cp");
    	cmdLine.addArgument(h+separator+j+separator+getPath());
    	cmdLine.addArgument(getTestPath());
    	CommandLine cmdLine2 = new CommandLine("java");
    	cmdLine2.addArgument("-cp");
    	cmdLine2.addArgument(h+separator+j+separator+getPath());
    	cmdLine2.addArgument("org.junit.runner.JUnitCore");
    	cmdLine2.addArgument("MainTest");
    	try {
			executor.execute(cmdLine);
			executor.execute(cmdLine2);
    	
    	} catch (IOException e) {
    		if(watchdog.killedProcess())
				throw new ErrorPruebaException("El proceso de prueba ha excedido el tiempo máximo permitido");
			throw new ErrorPruebaException(outputStream.toString().replace(getPath(), "")); 
    	}
    	return outputStream.toString();
	}
	
	private String getCodePath(){
		return new File(SystemDefaultProperties.ROOT_FOLDER+"/"+job.getId()+"/"+SystemDefaultProperties.MAIN_FILE).getAbsolutePath();
	}
	
	private String getTestPath(){
		return new File(SystemDefaultProperties.ROOT_FOLDER+"/"+job.getId()+"/"+SystemDefaultProperties.TEST_FILE).getAbsolutePath();
	}
	
	private String getPath(){
		return getParent().getAbsolutePath();
	}
	
	private File getParent(){
		return new File(SystemDefaultProperties.ROOT_FOLDER+"/"+job.getId());
	}
	
    public static void main( String[] args ) throws ErrorProgramaException{
    	String programa = 
    			"class Calculator {"
    			+"/*public static void main(String args[]){System.out.println(0/0);}*/"
    			+   "public int evaluate(String expression) {"
    			+  		"int sum = 0;"
    			+"for (String summand: expression.split(\"\\\\+\"))"
    			+"sum += Integer.valueOf(summand);"
    			+" return sum;"
    			+"}}";
    	
    	String test = 
    			"import static org.junit.Assert.assertEquals;"
    			+"import org.junit.Test;"
    			+"import java.util.concurrent.TimeUnit;"
    			+"public class MainTest {"
    			+	"@Test\n"
    			+	"public void evaluatesExpression() throws InterruptedException {"
    			+		"/*TimeUnit.SECONDS.sleep(100);*/"
    			+		"Calculator calculator = new Calculator();"
    			+		"int sum = calculator.evaluate(\"1+2+3\");"
    			+		"assertEquals(6, sum);"
    			+	"}"
    			+"};";	
    	//Programa aplicacion = new Programa(programa, test);
    	//System.out.println(aplicacion.ejecutar());
    	//System.out.println(aplicacion.probar());
		
    	
    	
    	
    }

}
