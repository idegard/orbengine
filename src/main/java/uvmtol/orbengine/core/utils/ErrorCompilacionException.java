package uvmtol.orbengine.core.utils;

public class ErrorCompilacionException extends ErrorProgramaException {
	public ErrorCompilacionException(String mensaje){
		super(mensaje);
	}
}
