package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private Environment env;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	  auth.inMemoryAuthentication().withUser(env.getProperty("orbengine.admin.username")).password(env.getProperty("orbengine.admin.password")).roles("ADMIN");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	  http
		.csrf().disable();
	  http.rememberMe();
	  http.authorizeRequests().anyRequest().permitAll()
		//.antMatchers("/api/vi/**").access("hasRole('ROLE_ADMIN')")
	 	.and().formLogin();
		
	}
}
