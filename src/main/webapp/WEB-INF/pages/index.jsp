<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Orbengine</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<style type="text/css" media="screen">
body { padding-top: 70px; }
#mainJava, #testJava {
	height: 300px;
}
</style>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Orbengine</a>
    </div>
	<p class="navbar-text navbar-right"> 
		<strong><c:out value="${usuario}"/></strong> | 	
		<c:choose>
		  <c:when test="${usuario.equals('anonymous')}">
		 	<a href="/login" class="navbar-link"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span></a>   
		  </c:when>
		  <c:otherwise>
		    <a href="/logout" class="navbar-link"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a>
		  </c:otherwise>
		</c:choose>
	</p>
  </div>
</nav>
<input id="job_id" type="hidden" value="${job}">
  <div class="row">
	<div class="col-xs-10 col-xs-offset-1">
	  <div class="panel panel-default">
		<div class="panel-heading">
		<span id="jobTitle">Nuevo trabajo</span>
		</div>


<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
    	<a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span id="mainTitle"></span>Main.java</a>
    </li>
    <li role="presentation">
    	<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><span id="testTitle"></span>MainTest.java</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
<!-- ------------------------------------------------------------ -->
<div class="panel-body" id="mainJava">/* package elquesea; // no agregar nombre de paquete */ 

import java.util.*; 
import java.lang.*; 
import java.io.*;
 
/* El nombre de la clase debe ser Main en caso de ser publica */ 
class Main{
 
	public static void main(String args[]){
		 //Tu c�digo aqu� 
	}
	
}</div>
<!-- ------------------------------------------------------------ --> 
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
<!-- ------------------------------------------------------------ -->
<div class="panel-body" id="testJava">/* package elquesea; // no agregar nombre de paquete */ 

import org.junit.*; 

/* NO cambiar el nombre de la clase */
public class MainTest { 
    
    @Test 
    public void example() {
        String sentidoDeLaVida = "42";
        Assert.assertEquals(2, sentidoDeLaVida.length());
    }
}</div>
<!-- ------------------------------------------------------------ --> 
    </div>
  </div>

</div>

        <div class="panel-footer">
		  <pre class="well" id="resultado"></pre>
			<!-- Split button -->
			 <div class="btn-group">
			  <button id="ejecutar" type="button" class="btn btn-success">Ejecutar</button>
			  <button type="button" class="btn btn-success dropdown-toggle"
				 			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			  <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span></button>
			  <ul class="dropdown-menu">
				<li><a id="compilar" href="#">Compilar</a></li>
				<li><a id="test" href="#">Test</a></li>
			  </ul>
			 </div>
		</div>
	</div>
  </div>
</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.3/ace.js"
		type="text/javascript" charset="utf-8"></script>
	<script>
    var editor = ace.edit("mainJava");
    editor.setTheme("ace/theme/eclipse");
    editor.getSession().setMode("ace/mode/java");
    
    var editor2 = ace.edit("testJava");
    editor2.setTheme("ace/theme/eclipse");
    editor2.getSession().setMode("ace/mode/java");
        
    if($( "#job_id" ).val()){
    	$( "#mainTitle" ).html('<i class="fa fa-spin fa-spinner"></i>');
    	$( "#testTitle" ).html('<i class="fa fa-spin fa-spinner"></i>');
    	editor.setValue('');
	    editor2.setValue('');
    	$.ajax({
	          type: "GET",
	          url: 'api/v1/job/'+$("#job_id").val(),
	          success: function(data) {
	              $("#jobTitle").html(data.job.id+" | by: "+data.job.author);
	              editor.setValue(data.job.code);
	              editor2.setValue(data.job.test);
	              $( "#resultado" ).html(data.response);
	          },
	          error: function(data){
	          	//error
	          },
	          complete: function(){
	          	$( "#mainTitle" ).html('');
    			$( "#testTitle" ).html('');
	          }
	        });
    }
    
    var consulta = function(accion){
    	$( "#resultado" ).html('<i class="fa fa-spin fa-spinner fa-2x"></i>');
    	if($( "#job_id" ).val()){
    		$.ajax({
	          type: "PUT",
	          url: 'api/v1/job/'+$("#job_id").val()+'?action='+accion,
	          data: JSON.stringify({ code: editor.getValue(), test: editor2.getValue() }),
	          contentType: "application/json",
	          dataType: "json",
	          success: function(data) {
	               $("#jobTitle").html(data.job.id+" | by: "+data.job.author);
	              $( "#resultado" ).html(data.response);
	              $( "#job_id" ).val(data.job.id);
	          },
	          error: function(data){
	          	  $( "#resultado" ).html(data.responseJSON.response);
	          }
	        });
    	}else{
    		$.ajax({
	          type: "POST",
	          url: 'api/v1/job?action='+accion,
	          data: JSON.stringify({ code: editor.getValue(), test: editor2.getValue() }),
	          contentType: "application/json",
	          dataType: "json",
	          success: function(data) {
	          	  $("#jobTitle").html(data.job.id+" | by: "+data.job.author);
	              $( "#resultado" ).html(data.response);
	              $( "#job_id" ).val(data.job.id);
	          },
	          error: function(data){
	          	  $("#jobTitle").html(data.job.id+" | by: "+data.job.author);
	              $( "#resultado" ).html(data.responseJSON.response);
	              $( "#job_id" ).val(data.job.id);
	          }
	        });
    	}
    	
    };
    $( "#ejecutar" ).click(function() {
    	consulta("execute");
    });
    $( "#compilar" ).click(function() {
    	consulta("compilar");
    });
    $( "#test" ).click(function() {
    	consulta("test");
    });
	</script>
	
</body>
</html>
